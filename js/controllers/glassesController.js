// render list of glasses from dataGlasses
export let renderGlasses = (list) => {
  let contentHTML = "";

  list.forEach((element) => {
    contentHTML += `
          <span class = "col-4">
            <img id= "${element.id}" src = "${element.src}" alt= "" width= "100%" class="vglasses__items"></img>
          </span>`;
  });
  document.getElementById("vglassesList").innerHTML = contentHTML;
};

export let showGlasses = (data) => {
  // way 1
  // get all glasses img
  let glassesList = document.querySelectorAll(".vglasses__items");
  // let glassesInformation = "";
  // loop each object and list this object when user click
  for (const glasses of glassesList) {
    glasses.addEventListener("click", () => {
      // console.log("glasses: ", glasses);

      // // find object in the array which user click it
      let object = data.find((item) => item.id == glasses.id);
      console.log("object: ", object);

      // Search id of object in array, it will be printed out if object'id is exactly
      if (object) {
        // Glasses image on avatar
        contentAvatar(object);

        // add to display class inside "vglasses__info" class to show this tags on screen
        document.getElementById("glassesInfo").classList.add("d-block");

        // Setup content for the tag
        glassesInformation(object);
      }
      document.getElementById("avatar").innerHTML = contentAvatar(object);
      document.getElementById("glassesInfo").innerHTML =
        glassesInformation(object);
    });
  }

  // way 2:  similar to "forEach"
  // glassesList.forEach((item) => {
  //   item.addEventListener("click", () => {
  //     console.log("item: ", item);
  //     var contentAvatar = `<img id="#glasses" src="${item.virtualImg}" alt ="" width= "100%"></img>`;
  //   });
  // });
};

let contentAvatar = (data) => {
  return `<img id="${data.id}" src="${data.virtualImg}" alt ="" width= "100%" class="image__avatar"></img>`;
};

let glassesInformation = (data) => {
  return `
  <h5>${data.name} - ${data.brand} (${data.color})</h5>
  <span class="bg-danger p-1 rounded">$${data.price}</span> <span class="text-success ml-1"> Stocking</span>
  <p class="mt-3">${data.description}</p>
  `;
};
