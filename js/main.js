import { dataGlasses } from "./asset/data.js";
import { renderGlasses, showGlasses } from "./controllers/glassesController.js";

renderGlasses(dataGlasses);

showGlasses(dataGlasses);
